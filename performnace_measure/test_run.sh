#!/usr/bin/env bash

set -e

MYPATH="$(dirname $(dirname $(realpath $0)))"
export PRMS="${MYPATH}/scripts/"

PRMFILE="$PRMS/parm_new_ace.prm"
RTFFILE="$PRMS/pdbamino_new.rtf"
PARAMSFILE="$PRMS/atoms.0.0.6.prm.ms.3cap+0.5ace.Hr0rec.params"

MCM="mcm_protocol_1"

DTRANS=0.1
DROT=0.1
DDIH=1.57
NSTEPS=20
if [[ $1 != "" ]]; then
    NSTEPS=$1
fi

# Print the command before executing
cd "$MYPATH/output/"
set -o xtrace
"$MCM" rec_min.pdb rec_min.psf rec_min.mol2 lig.json lig_rmsd.pdb complex.pdb rigid_cluster1.prm "$PRMFILE" "$RTFFILE" "$PARAMSFILE" mcm-traj.pdb mcm-min.pdb mcm-min.dat --seed=12345 --steps=$NSTEPS -r $DROT -t $DTRANS -d $DDIH --kt 2.0
