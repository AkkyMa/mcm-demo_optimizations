#!/usr/bin/env bash

PROJECT_DIR="$(dirname $(dirname $(realpath $0)))"

#_________________________USER_DEFINED_VARIABLES_____________________________
#compilers in format <name>=<executable>
gcc=/usr/bin/gcc
icc=${HOME}/intel/compilers_and_libraries/linux/bin/intel64/icc

#default options of cmake which will be applied for every specified project
default_options="-DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME -DDIR_PROFINFO=${PROJECT_DIR}/prof-info"
#list of project names for rebuilding
projects=(libmol2 libsampling)
#directories of specified projects in format <name>_dir=<directory>
libmol2_dir="${HOME}/libmol2"
libsampling_dir="${HOME}/libsampling"
#____________________________________________________________________________


USAGE="Usage: ./rebuild.sh [options] compiler [cmake_options] [project cmake_options]..."
HELP="$USAGE"'
Script for rebuilding multiple projects using cmake. Before using script, please specify your options on its head in '\
'area marked as "USER_DEFINED_VARIABLES".

options:
    -h,  --help               Print this message.
    -t,  --test               Make testing of projects after rebuilding. It assumes that in every project
                              available "BUILD_TESTS" option and "make test" command in built directory.
    -rf, --rebuild-full       Rebuild all projects previously deleting build directory. Default option.
    -rp, --rebuild-partial    Rebuild all projects without previously deleting build directory.
    -pi, --print_important    Minimize output information. Default option.
    -pa, --print_all          Print all output that script produce.'

regex_tests=$'([0-9]+% tests passed, [0-9]+ tests failed out of [0-9]+)

Total Test time \(real\) = [[:print:]]*(

(The following tests FAILED:(
[[:blank:]]*[0-9]+ - test_[[:graph:]]+ \(Failed\))+))?'

tmpfile=$(mktemp /tmp/rebuild-script.XXXXXX)

echo_error() {
    local print_string="\e[31m$1\e[0m"
    >&2 echo -e ${print_string}
    exit 1
}

contains() {
    for element in $2
    do
        if [[ "$1" == "$element" ]]; then
            return 0
        fi
    done
    return 1
}

progress_bar_build() {
    local regex="^\[[ ]*([0-9]{1,3})%\].*$"
    local length=40

    while IFS= read -r line
    do
        if [[ "$line" =~ ${regex} ]]; then
            local percent=${BASH_REMATCH[1]}
            local fill=$(( $percent * $length / 100 ))

            echo -n "|"
            for (( i=0; i<${fill}; i++ ))
            do
                echo -n "#"
            done
            for (( i=${fill}; i<${length}; i++ ))
            do
                echo -n "-"
            done
            echo -n "|"

            echo -ne " [$percent%]\r"
        fi
    done
    for (( i=0; i<${length}+20; i++ ))
    do
        echo -n " "
    done
    echo -ne "\r"
}

progress_bar_test() {
    local regex="^[[:blank:]]*([[:digit:]]+)/([[:digit:]]+)"
    local length=40

    while IFS= read -r line
    do
        if [[ "$line" =~ ${regex} ]]; then
            local step=${BASH_REMATCH[1]}
            local total=${BASH_REMATCH[2]}
            local fill=$(( $step * $length / $total ))

            echo -ne "\r"
            echo -n "|"
            for (( i=0; i<${fill}; i++ ))
            do
                echo -n "#"
            done
            for (( i=${fill}; i<${length}; i++ ))
            do
                echo -n "-"
            done
            echo -n "|"

            echo -n " [$step / $total]"
        fi
    done
    echo -ne "\r"
    for (( i=0; i<length+total/5+30; i++ ))
    do
        echo -n " "
    done
    echo -ne "\r"
}

check_projects() {
    for project in ${projects[@]}
    do
        dir_name="${project}_dir"
        if [[ ! -d "${!dir_name}" ]]; then
            echo_error "for project \"${project}\" directory unspecified"
        fi
    done
}
check_projects

parse_tests() {
  local str="$1"
  local arr1=()
  local arr2=()

  local regex1=$'Running suite\(s\): [^\n]+
(([[:print:]]+\n)+)'

  local regex2=$'[[:digit:]]+% tests passed, [[:digit:]]+ tests failed out of [[:digit:]]+'

  local regex3=$'[[:blank:]]*([[:digit:]]+ - test_[[:graph:]]+)'

  while [[ "$str" =~ $regex1 ]]; do
    str=${str#*"${BASH_REMATCH[0]}"}
    arr1+=("${BASH_REMATCH[1]}")
  done

  [[ "$str" =~ $regex2 ]]
  str=${str#*"${BASH_REMATCH[0]}"}
  echo "${BASH_REMATCH[0]}"

  while [[ "$str" =~ $regex3 ]]; do
    str=${str#*"${BASH_REMATCH[0]}"}
    arr2+=("${BASH_REMATCH[1]}")
  done

  for (( i=0; i < ${#arr1[@]}; i++ )); do
    echo "${arr2[i]}"
    echo -n "${arr1[i]}"
  done
}

test=false
print_mode=important
rebuild_mode=full
while [[ $# -gt 0 ]]
do
    case $1 in
    -h|--help)
        echo "${HELP}"
        exit ;;
    -t|--test)
        test=true
        ;;
    -r*|--rebuild-*)
        case $1 in
        -rf|--rebuild-full)
            rebuild_mode=full ;;
        -rp|--rebuild_partial)
            rebuild_mode=part ;;
        *)
            >&2 echo "Unrecognized rebuild mode flag $1"
            exit 1;;
        esac
        ;;
    -p*|--print-*)
        case $1 in
        -pa|--print-all)
            print_mode=all ;;
        -pi|--print_important)
            print_mode=important ;;
        *)
            >&2 echo "Unrecognized print mode flag $1"
            exit 1 ;;
        esac
        ;;
    --)
      shift
      break ;;
    *)
        break
        ;;
    esac
    shift
done

case ${print_mode} in
all)
    important_out="&1"
    rest_out="&1"
    ;;
important)
    important_out="&1"
    rest_out="/dev/null"
    ;;
esac

if [[ $1 == "" ]]; then
    >&2 echo "Compiler not specified"
    echo "${USAGE}"
    exit 1
fi
compiler=$1
if [[ "${compiler}" == -* ]]; then
    >&2 echo "Wrong compiler name \"$compiler\""
    exit 1
elif [[ "${!compiler}" == "" ]]; then
    >&2 echo "Unknown compiler \"$1\" specified"
    exit 1
fi
shift

export CC=${!compiler}
export CTEST_OUTPUT_ON_FAILURE=1

declare -A cmake_opts
project="all"
for word in "$@"
do
    case ${word} in
        -D[[:alnum:]_-]*=*)
            cmake_opts["$project"]+="$word "
            ;;
        [^-]?*)
            if ! contains "$word" "${projects[*]}"; then
                2>&1 echo "Unknown project \"$word\" specified"
                exit 1
            fi
            project="$word"
            ;;
        *)
            2>&1 echo "Unrecognized argument $word"
            exit 1 ;;
    esac
done
cmake_flags=$@

for project in "${projects[@]}"
do
    options="${default_options} ${cmake_opts['all']} ${cmake_opts[${project}]}"

    eval "echo -- \"${project}\" building start >${important_out}"

    dir_name=${project}_dir
    dir=${!dir_name}
    cd ${dir}
    if [[ ${rebuild_mode} == full ]]
    then
        rm -r build
        mkdir build
    fi
    cd build

    eval "cmake ${options} -DBUILD_TESTS=${test} .. >${rest_out}"
    if [[ $? != 0 ]]; then
       echo_error "-- \e[31m\"cmake\" command ended with error"
    fi

    if [[ "$print_mode" = "all" ]]; then
        make
        if [[ $? != 0 ]]; then
            echo_error "-- \e[31m\"make\" command ended with error"
        fi
    elif [[ "$print_mode" = "important" ]]; then
        make 2>"$tmpfile" | progress_bar_build
        if [[ $? != 0 ]]; then
            cat "$tmpfile"
            echo_error "-- \e[31m\"make\" command ended with error"
        fi
    fi

    eval "echo -- \"${project}\" built >${important_out}"

    eval "make install >${rest_out}"
    if [[ $? != 0 ]]; then
       echo_error "-- \"${project}\" installing ended with an error"
    fi

    eval "echo -- \"${project}\" installed >${important_out}"

    if ${test}; then
        if [[ ${print_mode} == important ]]; then
            eval "echo -- tests running >${important_out}"
            make test 2>&1 | tee "$tmpfile" | progress_bar_test
            out="$(cat ${tmpfile})"
            parse_tests "${out}"
        elif [[ ${print_mode} == all ]]; then
            eval "make test >${rest_out}"
        fi
    fi
done
