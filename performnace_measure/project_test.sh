#!/usr/bin/env bash

#_________________________USER_DEFINED_VARIABLES_____________________________
default_compilers=(gcc icc)
default_options="-DUNSAFE_OPTIMIZATIONS -DNATIVE_ARCH"
default_icc_options="-DUSE_PROFINFO -DPARALLELIZATION"
export LD_LIBRARY_PATH="${HOME}/intel/compilers_and_libraries/linux/lib/intel64:$LD_LIBRARY_PATH"
#____________________________________________________________________________

MYPATH="$(dirname $(realpath $0))"

tmpfile=$(mktemp /tmp/performance-test.XXXXXX)
errfile=$(mktemp /tmp/performance-test-error.XXXXXX)
resultfile="$MYPATH/result"
touch ${resultfile}

export LC_NUMERIC="en_US.UTF-8"

rebuild="${MYPATH}/rebuild.sh"
test_run="${MYPATH}/test_run.sh"
sort_result="python3 ${MYPATH}/sort_result.py"

USAGE="./performance_test.sh [options] [--] [test_options [compiler [test_options]]...]"
HELP="${USAGE}"'

Script for testing projects with different variants of cmake options

options:
    -h, --help              Print this message
    -t, --test              Test every build.
    -n, -s, --steps NUM     Specify number of steps for each run to NUM. Default is 100. Specify to 0 to disable test runs
    -r, --repeats NUM       Specify number of repeats for each run to NUM. Default is 1.

test_options:
-D<var>=<value> - fixed test option. Passes to each rebuild as given.
-D<var> - variable test option. In each run it can take value ON or OFF. Given script goes through all possible '\
'combinations of test options.
[ fixed_test_options test_option ] Conditional test_option. In this case <test_option> which can be fixed or variable '\
'participate in testing only when all <fixed_test_options> has specified value

Test options specified before specifying any compiler will be applied to all compilers. Otherwise, test options will be '\
'applied for last specified compiler. You also can use pseudo compiler name "all" to specify test options for all compilers.

On rebuilding options sort in next orders:
1. Fixed options, variable options, conditional fixed options, conditional variable options
2. Options for all compilers, options for certain compiler

Note that in head of script you can specify default_compilers and default_options that will be applied for every '\
'compiler. Default options will be used if no test options or compiler will be specified manually on script run.'

regex_fix_option="^-D[[:graph:]]+=[[:graph:]]+$"
regex_var_option="^(-D[^=[:blank:]]*)$"
regex_test_option="^-.*$|^\[$|^\]$"

regex_tests=\
$'(([0-9]+% tests passed, [0-9]+ tests failed out of [0-9]+)(
The following tests FAILED:(
[[:blank:]]*[0-9]+ - test_[[:graph:]]+ \(Failed\))+)?)'

progress_bar() {
    local regex="^Step ([0-9]+) / ([0-9]+)"
    local length=40
    local repeats_str=""
    if [[ $# -gt 0 ]]; then
        local repeats_str="[$1 / $2]"
    fi

    while IFS= read -r line
    do
        if [[ "$line" =~ ${regex} ]]; then
            n1=${BASH_REMATCH[1]}
            n2=${BASH_REMATCH[2]}
            local fill=$(( n1 * $length / n2 ))

            echo -n "|"
            for (( i=0; i<${fill}; i++ ))
            do
                echo -n "#"
            done
            for (( i=${fill}; i<${length}; i++ ))
            do
                echo -n "-"
            done
            echo -n "|"

            echo -ne " [$n1 / $n2] ${repeats_str}\r"
        fi
    done
    for (( i=0; i<length+n2/2+20; i++ ))
    do
        echo -n " "
    done
    echo -ne "\r"
}

contains() {
    for element in $2
    do
        if [[ "$1" == "$element" ]]; then
            return 0
        fi
    done
    return 1
}

echo_error() {
    >&2 echo "[ERROR] $1"
    exit 1
}

parse_params() {
    while [[ $# -gt 0 ]]
    do
        if [[ $1 =~ ${regex_fix_option} ]]
        then
            fix_options["${compiler}"]+="$1 "
        elif [[ $1 =~ ${regex_var_option} ]]
        then
            var_options["${compiler}"]+="$1 "
        elif [[ $1 == '[' ]]
        then
            shift
            cond_option=""
            last_option=""
            while [[ $1 != ']' ]]
            do
                if [[ $# == 0 ]]; then
                    echo_error "Conditional option was not completed by ']' symbol"
                fi
                if [[ last_option =~ $regex_var_option ]]; then
                    echo_error "Conditional expression was not completed after variable option"
                fi

                cond_option+="$last_option "

                if [[ $1 =~ $regex_test_option ]]
                then
                    last_option="$1"
                else
                    echo_error "Unrecognized option in conditional option $1"
                fi
                shift
            done

            if [[ "$last_option" =~ ${regex_fix_option} ]]; then
                count=$(( cond_fix_options["${compiler}_count"] + 1 ))
                cond_fix_options["${compiler}_count"]=${count}
                cond_fix_options["${compiler}_${count}_cond"]=${cond_option}
                cond_fix_options["${compiler}_${count}_opt"]=${last_option}
            elif [[ "$last_option" =~ ${regex_var_option} ]]; then
                count=$(( cond_var_options["${compiler}_count"] + 1 ))
                cond_var_options["${compiler}_count"]=${count}
                cond_var_options["${compiler}_${count}_cond"]=${cond_option}
                cond_var_options["${compiler}_${count}_opt"]=${last_option}
            fi
        elif [[ $1 == ']' ]]
        then
            echo_error "Conditional option was completed without beginning"
        else
            echo_error "Illegal option $1"
        fi
        shift
    done
}

declare -A fix_options var_options cond_fix_options cond_var_options

test=false
steps=100
repeats=1
while [[ $# -gt 0 ]]
do
    case $1 in
        -h|--help)
            echo "${HELP}"
            exit ;;
        -t|--test)
            test=true
            ;;
        -n|-s|--steps)
            if [[ -z $2 ]]; then
                echo_error "Number of steps not specified after \"$1\" option"
            elif [[ ! $2 =~ ^[[:digit:]]+$ ]]; then
                echo_error "Wrong step number \"$2\" specified after \"$1\" option"
            elif [[ $2 -lt 0 ]]; then
                echo_error "Steps number cannot be less zero"
            fi
            steps=$2
            shift
            ;;
        -r|--repeats)
            if [[ -z $2 ]]; then
                echo_error "Not specidied number of repeats after \"$1\" option"
            elif [[ ! $2 =~ ^[[:digit:]]+$ ]]; then
                echo_error "Wrong repeats number \"$2\" specified after \"$1\" option"
            elif [[ $2 -le 0 ]]; then
                echo_error "Repeats number cannot be less than or equal to zero"
            fi
            repeats=$2
            shift
            ;;
        --)
            shift
            break ;;
        *)
            break ;;
    esac
    shift
done

if [[ -n $1 ]]
then
    compilers=()
    compiler="all"
    while [[ $# -gt 0 ]]
    do
        options=""
        while [[ $# -gt 0 && $1 =~ $regex_test_option ]]
        do
            options+="$1 "
            shift
        done

        parse_params ${options}

        if [[ $# -gt 0 ]]
        then
            compiler=$1
            if ! contains "$compiler" "all ${compilers[@]}"; then
                compilers+=" ${compiler}"
            fi
            shift
        fi
    done
    if [[ -z ${compilers[@]} ]]; then
        echo_error "No one compiler specified"
    fi
else
    compilers=${default_compilers[@]}
    compiler="all"
    parse_params ${default_options}
    for compiler in ${default_compilers[@]}
    do
        options_name=default_${compiler}_options
        parse_params ${!options_name}
    done
fi

run_strings=()
gen_run_strings() {
    add_run_string() {
        run_strings+=( "$1" )
    }

    condition_corresponds() {
        correspond=true
        for condition in $1
        do
            contain=false
            for option in $2
            do
                if [[ $condition == $option ]]; then
                    contain=true
                    break
                fi
            done
            if ! $contain ; then
                correspond=false
                break
            fi
        done

        result=${correspond}
    }

    gen_cond_var_options() {
        local count=cond_var_options["${3}_count"]
        local i=$2
        while [[ $i -le $count ]]
        do
            local index="${3}_${i}"
            local condition=${cond_var_options["${index}_cond"]}
            condition_corresponds "$condition" "$1"
            if $result; then
                local option=${cond_var_options["${index}_opt"]}
                gen_cond_var_options "$1 $option=OFF" $(( i + 1 )) $3
                gen_cond_var_options "$1 $option=ON"  $(( i + 1 )) $3
                break
            fi
            i=$(( $i + 1 ))
        done
        if [[ $i -gt $count ]]; then
            if [[ $3 == all ]]; then
                gen_cond_var_options "$1" 1 $compiler
            else
                add_run_string "$1"
            fi
        fi
    }

    gen_cond_options() {
        local run_string=$1

        local count=${cond_fix_options["all_count"]}
        for (( i=1; i <= count; i++ ))
        do
            local index="all_${i}"
            local conditions=${cond_fix_options["${index}_cond"]}
            condition_corresponds "${conditions}" "${run_string}"
            if $result; then
                local option="cond_fix_options[${index}_opt]"
                run_string+=" ${option}"
            fi
        done

        local count=${cond_fix_options["${compiler}_count"]}
        for (( i=1; i <= count; i++ ))
        do
            local index="${compiler}_${i}"
            local conditions=${cond_fix_options["${index}_cond"]}
            condition_corresponds "${conditions}" "${run_string}"
            if $result; then
                local option=${cond_fix_options["${index}_opt"]}
                run_string+=" ${option}"
            fi
        done

        gen_cond_var_options "$run_string" 1 all
    }

    gen_var_options() {
        if [[ $# -gt 1 ]]; then
            gen_var_options "$1 $2=OFF" ${@:3}
            gen_var_options "$1 $2=ON"  ${@:3}
        else
            gen_cond_options "$1"
        fi
    }

    for compiler in ${compilers[@]}
    do
        local run_string="${compiler}"
        local run_string+=" ${fix_options['all']} ${fix_options[$compiler]}"

        gen_var_options "$run_string" ${var_options["all"]} ${var_options["$compiler"]}
    done
}

gen_run_strings

: > ${resultfile}
for run_string in "${run_strings[@]}"
do
    echo ${run_string}
    echo "${run_string}" >> ${resultfile}
    cmd=""
    cmd+=" ${rebuild} -pi -rf"
    if ${test}; then
        cmd+=" -t"
    fi
    cmd+=" ${run_string}"

    ${cmd} 2>&1 | tee ${tmpfile}

    if [[ $? != 0 ]]; then
        cat ${tmpfile}
        exit 1
    fi

    if ${test}; then
        out=$(cat ${tmpfile})
        while [[ "$out" =~ $regex_tests ]]; do
            echo "${BASH_REMATCH[2]}" >> ${resultfile}
            out=${out#*"${BASH_REMATCH[1]}"}
        done
    fi

    if [[ $steps -gt 0 ]]; then
    echo "-- Test run start"
    fi
    exec_time=0
    for (( i=0; i<$repeats; i++ ))
    do
        if [[ ${repeats} -gt 1 ]]; then
            repeat_arg1=${i}
            repeat_arg2=${repeats}
        fi
        /usr/bin/time -f %e -o ${tmpfile} stdbuf -o0 ${test_run} ${steps} 2>${errfile} | progress_bar ${repeat_arg1} ${repeat_arg2}
        if [[ $? == 0 ]]; then
            out=$(cat ${tmpfile})
            exec_time=$(bc -l <<< "$exec_time + $out")
        else
            cat ${errfile}
            echo -e "\e[31mtest run aborted with code $?\e[0m"
            exit 1
        fi
    done
    if [[ $steps -gt 0 ]]; then
      exec_time=$(bc -l <<< "$exec_time / $repeats")
      printf "Execution time(s): %.2f\n" ${exec_time} | tee -a ${resultfile}
      echo "" | tee -a ${resultfile}
    fi
done

if [[ $steps -gt 0 ]]; then
  ${sort_result}
fi

echo ""
cat ${resultfile}
