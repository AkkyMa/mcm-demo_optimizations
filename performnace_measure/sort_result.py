from math import ceil

file = open("result", "r")

results=list()
lines=list()

for line in file.read().splitlines():
    if line != '':
        lines.append(line)
    else:
        results.append(lines)
        lines=list()
if lines:
    results.append(lines)

def key(arr):
    return float(arr[-1].split()[-1])

results=sorted(results, key=key, reverse=True)

file.close()

top = float(results[0][-1].split(' ')[-1])
for result in results:
    current = float(result[-1].split(' ')[-1])
    percent = ceil(current / top * 100)
    result[-1] += f" ({percent}%)"

file = open("result", "w")

for i, result in enumerate(results):
    if (i != 0):
        file.write("\n")
    for line in result:
        file.write(line+"\n")

file.close()

arr1=[1, 2]
arr2=[]