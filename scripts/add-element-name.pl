#!/usr/bin/perl -w

if(@ARGV!=2){
    die"usage: $0 pdb-file-in pdb-file-out\n";
}

$pdbfile=shift(@ARGV);
$outfile=shift(@ARGV);

open IN,"<$pdbfile";
open OUT,">$outfile";
while(<IN>){
    if(m/ATOM|HETATM/){
	$element=substr($_, 12, 2);
	if($element eq "NH" || $element eq "HN" || $element eq "HC"){#exception for hydrogens with long names
	    $element="H";
	}

	#printf"element: $element\n";
	$line=substr($_, 0, 54);
	#$segid=substr($_, 72, 4);

	$occupancy=substr($_, 54, 6);
	$temp_factor=substr($_, 60, 6);

	$size=length($_);
	if($size<71){
	    $segid=" ";
	}else{
	    $segid=substr($_, 72, 4);
	    chomp($segid);
	}
	#$segid="X";
	#printf"segid: $segid:\n";
	printf OUT "$line%6.2f%6.2f      %-4s%2s\n", $occupancy, $temp_factor, $segid, $element;
    }else{
        printf OUT $_;
    }

}
close IN;
close OUT;
printf"output is saved to file: $outfile\n";
