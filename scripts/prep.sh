#!/bin/bash

set -euo pipefail

MYPATH=$(cd $(dirname $0) && pwd)

PRMFILE="$MYPATH/parm_new_ace.prm"
RTFFILE="$MYPATH/pdbamino_new.rtf"

"$MYPATH/small-mol-preparation.sh" -p "${PRMFILE}" -t "${RTFFILE}" -e lig_rmsd.pdb -n rec.pdb lig.json
