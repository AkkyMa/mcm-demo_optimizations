#!/bin/bash

args=("$@");

grep "active torsions" ${args[0]} | awk '{print $2+1}' > ${args[2]}

num=0;

while read line
do

	  c1=$(echo "$line" | cut -d' ' -f 1);

	  if [ "$c1" == "ROOT" ] || [ "$c1" == "ENDROOT" ] || [ "$c1" == "BRANCH" ] || [ "$c1" == "ENDBRANCH" ]; then

		if [ $num -ne 0 ]; then
			echo -ne "$num " >> ${args[2]};
			for (( i = 1 ; i <= $num ; i++ ))
			do
				numval=$((${atm[${i}]}+${args[3]}));
				
				if [ $i -ne $num ]; then 
					echo -ne "$numval " >> ${args[2]};
				else
					echo $numval >> ${args[2]};
				fi
			done
			num=0;
		fi
	  fi

	  if [ "$c1" == "ATOM" ]; then
		num=$((num+1));

		x=$(echo "$line" | awk '{print $6}');
		y=$(echo "$line" | awk '{print $7}');
		z=$(echo "$line" | awk '{print $8}');




		# read the pdb file and find which atom is that 
		
		while read linepdb
		do

			 a1=$(echo "$linepdb" | cut -d' ' -f 1);

			 if [ "$a1" == "ATOM" ]; then

				 ax=$(echo "$linepdb" | awk '{print $6}');
                		 ay=$(echo "$linepdb" | awk '{print $7}');
                		 az=$(echo "$linepdb" | awk '{print $8}');
				
			 fi


			if [ "$x" == "$ax" ] && [ "$y" == "$ay" ] && [ "$z" == "$az" ]; then

				c2=$(echo "$linepdb" | awk '{print $2}');
				atm["$num"]="$c2";
				break;
			fi

		done < ${args[1]}	

	  fi
done < ${args[0]}



grep "ENDBRANCH"  ${args[0]} |  awk '{print $2 " " $3}' >> Edg.txt

while read line
do

	v1=$(echo "$line" | awk '{print $1}');
        v2=$(echo "$line" | awk '{print $2}');


	while read linepdbq
	do

		w1=$(echo "$linepdbq" | cut -d' ' -f 1);
		w2=$(echo "$linepdbq" | awk '{print $2}');


		if [ "$w1" == "ATOM" ] && [ "$w2" == "$v1" ]; then

			 x1=$(echo "$linepdbq" | awk '{print $6}');
                         y1=$(echo "$linepdbq" | awk '{print $7}');
                         z1=$(echo "$linepdbq" | awk '{print $8}');
		fi

		if [ "$w1" == "ATOM" ] && [ "$w2" == "$v2" ]; then

			 x2=$(echo "$linepdbq" | awk '{print $6}');
                         y2=$(echo "$linepdbq" | awk '{print $7}');
                         z2=$(echo "$linepdbq" | awk '{print $8}');			

		fi


	done < ${args[0]}


	while read linepdb
	do
		 a1=$(echo "$linepdb" | cut -d' ' -f 1);

		 if [ "$a1" == "ATOM" ]; then

                                 ax=$(echo "$linepdb" | awk '{print $6}');
                                 ay=$(echo "$linepdb" | awk '{print $7}');
                                 az=$(echo "$linepdb" | awk '{print $8}');

                 fi


		if [ "$x1" == "$ax" ] && [ "$y1" == "$ay" ] && [ "$z1" == "$az" ]; then

			num1=$(echo "$linepdb" | awk '{print $2}');
		fi

		if [ "$x2" == "$ax" ] && [ "$y2" == "$ay" ] && [ "$z2" == "$az" ]; then

			num2=$(echo "$linepdb" | awk '{print $2}');
                fi


	done < ${args[1]}

	num1=$(($num1+${args[3]}));
	num2=$(($num2+${args[3]}));

	echo "$num1  $num2 " >> ${args[2]}

done < Edg.txt
rm Edg.txt;
 
