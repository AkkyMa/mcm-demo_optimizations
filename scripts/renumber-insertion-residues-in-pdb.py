#!/usr/bin/python

import argparse

parser=argparse.ArgumentParser()
parser.add_argument("pdbfile")
args=parser.parse_args()

offset=0
first_atom_of_res=True
old_res_id=""
with open(args.pdbfile) as pf:
    for line in pf:

        line=line.rstrip()

        #check if current atom is the first residue atom
        res_id=line[22:27]
        if res_id != old_res_id:
            first_atom_of_res=True

        #if the current atom is the first residue atom
        if first_atom_of_res:
            insert_field=line[26:27]
            if str.isalpha(insert_field) :
                offset += 1
                #print insert_field
            first_atom_of_res=False
     
        if offset == 0:
            print line
        elif line[:4] == "ATOM" or line[:6] == "HETATM":
            res_num=int(line[22:26])
            #overwrite insertion field with a blank char
            print "%s%4d%s%s" % (line[:22], res_num+offset," ", line[27:]) 
        elif line[:3] == "TER":
            #don't print old residue number for TER field
            print line[:3]
        else:
            print line

        old_res_id=res_id
