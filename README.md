Installation
============

The following installation instruction was tested on Ubuntu 16.04.5 LTS.

Installing libmol2
------------------

Install prerequisites:

    sudo apt install build-essential git cmake libjansson4 libjansson-dev gperf check

Download the code:

    git clone https://bitbucket.org/bu-structure/libmol2.git && cd libmol2 && git checkout cd99f37c

Build and install it for the current user:

    mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME .. && make && make install

Installing libsampling
----------------------

Download the code:

    git clone https://bitbucket.org/abc-group/libsampling.git && cd libsampling && git checkout 0d9b2160

Build and install it for the current user:

    mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME .. && make && make install


Check that the executable is reachable:

    export PATH="$HOME/bin:$PATH"
    which mcm_protocol_1  # Should print /home/<yourusername>/bin/mcm_protocol_1

Installing psfgen
-----------------

Download recent precompiled version of NAMD (2.12 `Linux-x86_64-multicore`) from http://www.ks.uiuc.edu/Development/Download/download.cgi?UserID=&AccessCode=&ArchiveID=1501 (registration is required).

Extract the archive and copy the `psfgen` binary to your local directory:

    tar zxf NAMD_2.12_Linux-x86_64-multicore.tar.gz && cd NAMD_2.12_Linux-x86_64-multicore && cp psfgen "${HOME}/bin/"


Check that the executable is reachable:

    export PATH="$HOME/bin:$PATH"
    which psfgen  # Should print /home/<yourusername>/bin/psfgen

Installing additional prerequisites
-----------------------------------

    sudo apt install pymol openbabel

Go to the current directory and run:

    ./bootstrap.sh

Running this demo
=================

After installing all prerequisites, simply go to the current directory and run:

    export PATH="$HOME/bin:$PATH" && ./prepare.sh && ./run.sh

After some substantial time the optimization results would be available in the following files:

 - output/mcm-min.pdb: Final structure
 - output/mcm-min.dat: Eneregy breakdown of the final structure
 - output/mcm-traj.pdb: The whole trajectory of optimization process as multi-frame PDB of the complex


